FROM ibmcom/websphere-traditional:latest-ubi

# Labels consumed by OpenShift
LABEL io.k8s.description="WAS 9" \
	io.k8s.display-name="WAS 9.0.5" \
	io.openshift.expose-services="9043:https, 9443:https" \
	io.openshift.tags="was"
	
EXPOSE 9043
EXPOSE 9443

